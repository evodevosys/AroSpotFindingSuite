function varargout = review2(varargin)
    %% =============================================================
    %   Name:    reviewFISHClassification.m
    %   Version: 2.5.4, 9th Aug. 2014
    %   Authors:  Allison Wu, Scott Rifkin
    %   Command:  reviewFISHClassification(stackName*) *Optional Input
    %   Description: reviewFISHClassification.m is a gui to browse the results of the spot finding algorithm, estimate and/or correct errors, and retrain the classifier if specified.
    %       -  Input Argument:
    %               * stackName, or {dye}_{stackSuffix} pair e.g. tmr_Pos1.tif, tmr001.stk, tmr_001, tmr_Pos1
    %               * If absent, it will ask you for the stackName.
    %       - The program brings up 4 image panes and several buttons:
    %               * The big one on the left has the evaluated maxima arranged left to right, top to bottom in order of probability estimates predicted.
    %               * The big one on the right has the zoomable image centered around the potential spot.
    %               * Two smaller ones on the bottom left have zooms of the region around a spot with the raw data and a laplace filtered image of the same region.
    %               * The 7x7 spot context (along with neighboring slices) is shown in the middle (3D intensity histograms).
    %               * In the left image pane:
    %                       > Maxima that are classified as spots are bordered by blue.
    %                       > Maxima that are rejected as spots are bordered in yellow.
    %                       > Maxima that are in the training set have a cross on them.
    %                       > Maxima that are manually curated but not in the
    %                       training set have a single diagnol slash through them.
    %                       > The current maximum is marked by a red box.
    %                       > Maxima that are curated at this time are marked
    %                       with light blue border and cross/slash.
    %       - Possible actions:
    %               * Click on the grey background of the gui.
    %                       > If you are going to use keystrokes, it is necessary to focus the computer's attention on the gui.
    %                       > Clicking on the grey background changes the focus to the gui and makes the program interpret keystrokes as the gui tells it to.
    %               * Click the 'Done w/ specimen' button.
    %                       > Done fixing this specimen.
    %                       > Saves all the changes and move on to the next specimen.
    %               * Page up/down keys.  $
    %                       > The left image pane is 25x25 but often more potential spots are evaluated.  Page up and down move you up or down to the next page of potential spots.
    %               * Left/right/up/down arrow.
    %                       > Used to move around the left image pane.
    %               * Bad specimen toggle button. $
    %                       > If you don't like the looks of the specimen, flag it as bad and move on.
    %               * Click 'Good Spot' button
    %                       > If you're on a good spot:  this spot will be added to the training set. (With a light blue X)
    %                       > If you're on a bad spot: this spot will be curated to a good spot but it will not be added to the training set. (With a single light blue slash)
    %               * Click 'Not a Spot' button
    %                       > If you're on a bad spot:  this spot will be added to the training set. (With a light blue X)
    %                       > If you're on a good spot: this spot will be curated to a bad spot but it will not be added to the training set. (With a single light blue slash)
    %               * Click 'Add to training set' button
    %                       > Add the spot to the training set without changing the classification.
    %               * Toggle add corrected spot to trainingSet (On) or Not (off, default)
    %                       > Changes the behavior of the 'Good Spot' and 'Not a Spot' buttons to add to training set in addition to correcting (light blue X).
    %               * Scrollbar under the right image $
    %                       > Change the zoom of the right image.  The number under it displays the current zoom
    %               * Click 'Undo the Last Spot' button:
    %                       > undo the action on the last spot you curated
    %               * Click 'Undo All' button:
    %                       > clear all the unsaved (light blue) spot curation.
    %               * Toggle arrow to spot radio button $
    %                       > There is a little red arrow that points to the current spot in the right image.  This toggles it on and off if it is disturbing you.
    %               * Toggle On=Slice;Off=merge radio button $
    %                       > Changes the right image to just the slice that includes the spot (On) or a max merge of the stack (Off)
    %               * 'Redo classifySpots' button
    %                       > This retrains the random forest classifier with the new training set.
    %                       > Redo classifySpots on this specimen and output the new results to the GUI.
    %               * Checkbox in the lower right. $
    %                       > If checked, this means that the user has gone through and corrected this file and is satisfied with it.
    
    %
    %   Files required: the corresponding **_segStacks.mat, **_wormGaussianFit.mat, trainingSet_**.mat, **_spotStats.mat
    %   Files generated: overwrites all the files mentioned above except for **_segStacks.mat
    %
    %   Updates:
    %       - 2012 Aug 13th, small bug fixes
    %       - 2013 Mar 19th, small bug fixes
    %       - 2013 May 19th, fix 'index exceeds matrix' problem caused by
    %       including 'edge spots'.
    %		- 2014 Aug 9th, make redo mach Learn button run faster by not choosing variables and nFeatures again
    %       - 2015 March 24, change in displayImFull to speed it up
    %            imshow calls were taking a while. fix by plotting the right and lower left images (and surfs?)
    %            and then just changing the xlim and ylim on the relevant axis
    %            xlim and ylim work by specifying the outer corners (upper left, lower right) coordinates
    %            this needs some care:  if im is an image, to plot im(1:10, 34:43), then I need to specify
    %            xlim:  [34 43+1]-.5  ylim: [1 10+1]-.5
    
    %   Attribution: Rifkin SA., Identifying fluorescently labeled single molecules in image stacks using machine learning.  Methods Mol Biol. 2011;772:329-48.
    %   License: Apache 2.0, http://www.apache.org/licenses
    %   Website: http://www.biology.ucsd.edu/labs/rifkin/software/spotFindingSuite
    %   Email for comments, questions, bugs, requests:  sarifkin at ucsd dot edu
    %% =============================================================
    
    
    
    
    figH=900;
    figW=1800;
    figWH=[figW figH figW figH];
    figHandle=figure('Visible','off','Name','Review FISH Classification GUI','resize','on','Position',[0 0 figW figH],'Toolbar','none','Units','pixels','KeyPressFcn',@figure1_KeyPressFcn);
    
    
    % create structure of myHandles
    %http://www.mathworks.com/help/techdoc/ref/guidata.html
    myHandles = guihandles(figHandle);
    myHandles.figHandle=figHandle;
    
    myHandles.figWH=figWH;
    
    
    %%  Construct the components
    
    %%%%%%%%%% axes %%%%%%%%%%%%%%%%%
    
    
    myHandles.spotResults_axis=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[10 186 711 711]./figWH,'NextPlot','replacechildren','HitTest','on',...
        'ButtonDownFcn',@spotResults_ButtonDownFcn,'Tag','spotResults');
    
    myHandles.spotContext_axis=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[904 123 774 774]./figWH,'NextPlot','replace','HitTest','on',...
        'Tag','spotContext');
    
    myHandles.spotZoomLaplaceFiltered_axis=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[1 23 153 164]./figWH,'NextPlot','replace','HitTest','on',...
        'Tag','spotZoomLaplaceFiltered');
    
    myHandles.spotZoomRaw_axis=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[159 26 152 161]./figWH,'NextPlot','replace','HitTest','on',...
        'Tag','spotZoomRaw');
    
    
    myHandles.surfPlots_axes={};
    myHandles.surfPlots_axes{3}=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[721 312 184 150]./figWH,'NextPlot','replace','HitTest','on',...
        'Tag','surfPlus1');
    
    myHandles.surfPlots_axes{2}=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[721 159 184 150]./figWH,'NextPlot','replace','HitTest','on',...
        'Tag','surfInFocus');
    
    myHandles.surfPlots_axes{1}=axes('Parent',myHandles.figHandle,'Units','normalized',...
        'Position',[721 5 184 150]./figWH,'NextPlot','replace','HitTest','on',...
        'Tag','surfMinus1');
    
    
    %%%%%%%%%% uicontrols %%%%%%%%%%%%%%%%%
    
    myHandles.spotContextSlider_slider=uicontrol('Parent',myHandles.figHandle,'Style','slider',...
        'String','spotContextSlider','Tag','spotContextSlider','Units','normalized',...
        'Position',[1264 88 266 26]./figWH,'Min',1,'Max',6,'Value',2,...
        'Callback',@spotContextSlider_Callback,'CreateFcn',@spotContextSlider_CreateFcn);
    
    myHandles.goodSpot_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Good Spot','Tag','goodSpot_button','Units','normalized',...
        'Position',[747 709 125 45]./figWH,'Callback',@goodSpot_button_Callback,...
        'BackgroundColor',[.302 .745 .933],'ForegroundColor',[1 1 1],...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    
    myHandles.rejectedSpot_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Not a Spot','Tag','goodSpot_button','Units','normalized',...
        'Position',[747 664 125 45]./figWH,'Callback',@rejectedSpot_button_Callback,...
        'BackgroundColor',[.749 .749 0],'ForegroundColor',[ .039 .141 .416],...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.badWorm_button=uicontrol('Parent',myHandles.figHandle,'Style','radiobutton',...
        'String','Bad specimen','Value',0,'Tag','badWorm_button','Units','normalized',...
        'Position',[600 86 122 31]./figWH,'Callback',@badWorm_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.fileName_button=uicontrol('Parent',myHandles.figHandle,'Style','checkbox',...
        'String','file name done','Value',0,'Tag','fileName_button','Units','normalized',...
        'Position',[1364 0 179 21]./figWH,'Callback',@fileName_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.addToTrainingSet_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Add to training set','Tag','addToTrainingSet_button','Units','normalized',...
        'Position',[746 605 126 59]./figWH,'Callback',@addToTrainingSet_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.sliceMerge_button=uicontrol('Parent',myHandles.figHandle,'Style','radiobutton',...
        'String','On=Slice;Off=Merge','Value',1,'Tag','sliceMerge_button','Units','normalized',...
        'Position',[1069 60 169 27]./figWH,'Callback',@sliceMerge_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.redoMachLearn_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Redo classification','Tag','redoMachLearn_button','Units','normalized',...
        'Position',[1071 1 146 54]./figWH,'Callback',@redoMachLearn_button_Callback,...
        'BackgroundColor',[.635 .078 .184],'ForegroundColor',[1 1 1],...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.laplaceFilter_button=uicontrol('Parent',myHandles.figHandle,'Style','radiobutton',...
        'String','On=laplaceFiltered;Off=raw','Value',0,'Tag','laplaceFilter_button','Units','normalized',...
        'Position',[1300 36 216 20]./figWH,'Callback',@laplaceFilter_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.saveData_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Save data','Tag','saveData_button','Units','normalized',...
        'Position',[925 0 146 54]./figWH,'Callback',@saveData_button_Callback,...
        'BackgroundColor',[.302 .745 .933],'ForegroundColor',[0 0 0],...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.done_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Next one','Tag','done_button','Units','normalized',...
        'Position',[490 3 113 77]./figWH,'Callback',@done_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.allDone_button=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','All done','Tag','allDone_button','Units','normalized',...
        'Position',[599 3 113 77]./figWH,'Callback',@allDone_button_Callback);
    
    myHandles.undoTheLast=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Undo last spot','Tag','undoTheLast','Units','normalized',...
        'Position',[750 509 126 47]./figWH,'Callback',@undoTheLast_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.undoAll=uicontrol('Parent',myHandles.figHandle,'Style','pushbutton',...
        'String','Undo All','Tag','undoAll','Units','normalized',...
        'Position',[750 465 126 47]./figWH,'Callback',@undoAll_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.addCorrToTS_button=uicontrol('Parent',myHandles.figHandle,'Style','radiobutton',...
        'String','Add corrections to TS','Value',1,'Tag','addCorrToTS_button','Units','normalized',...
        'Position',[750 565 126 25]./figWH,'Callback',@addCorrToTS_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    myHandles.arrowSpot_button=uicontrol('Parent',myHandles.figHandle,'Style','radiobutton',...
        'String','Arrow to spot','Value',1,'Tag','arrowSpot_button','Units','normalized',...
        'Position',[935 61 121 25]./figWH,'Callback',@arrowSpot_button_Callback,...
        'KeyPressFcn',@figure1_KeyPressFcn);
    
    
    
    
    
    
    %%%%%%%%% text %%%%%%%%%%%%%%%%%%%
    myHandles.spotContextSlider_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[1262 67 269 26]./myHandles.figWH,'Tag','spotContextSlider_txt','String','Zoom');
    
    myHandles.currentSlice_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[321 89 142 18]./myHandles.figWH,'Tag','currentSlice_txt','String','currentSliceReporter');
    
    myHandles.nGoodSpots_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[315 160 146 21]./myHandles.figWH,'Tag','nGoodSpots_txt','String','nGoodSpots');
    
    myHandles.nRejectedSpots_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[493 160 127 21]./myHandles.figWH,'Tag','nRejectedSpots_txt','String','nRejectedSpots');
    
    myHandles.nRejectedToGood_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[493 123 127 21]./myHandles.figWH,'Tag','nRejectedToGood_txt','String','nRejectedToGood');
    
    myHandles.nGoodToRejected_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[315 123 149 21]./myHandles.figWH,'Tag','nGoodToRejected_txt','String','nGoodToRejected');
    
    myHandles.RandomForestResult_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[904 90 360 24]./myHandles.figWH,'Tag','RandomForestResult_txt','String','RandomForestResult');
    
    myHandles.iCurrentWorm_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[328 46 145 24]./myHandles.figWH,'Tag','iCurrentWorm_txt','String','currentWormReporter');
    
    myHandles.iCurrentSpot_worms_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[314 6 172 25]./myHandles.figWH,'Tag','iCurrentSpot_worms_txt','String','CurrentSpotWReporter');
    
    myHandles.spotPage_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[484 91 116 15]./myHandles.figWH,'Tag','spotPage_txt','String','Spot Page');
    
    myHandles.scdValue_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[1242 0 87 21]./myHandles.figWH,'Tag','scdValue_txt','String','scd value');
    
    myHandles.laplaceFilteredSpot_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[0 1 153 27]./myHandles.figWH,'Tag','laplaceFilteredSpot_txt','String','Laplace filtered');
    
    myHandles.rawSpot_txt=uicontrol(myHandles.figHandle,'Style','text','Units','normalized',...
        'Position',[159 2 147 26]./myHandles.figWH,'Tag','rawSpot_txt','String','Raw');
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    myHandles.spotsCurated=[];
    
    %import info and make spot pictures (background subtracted - essentially
    %the code from saveSpotPictures
    
    if isempty(varargin)
        stackName=input('Please enter the stack name (e.g. tmr001.stk, tmr_Pos1.tif, tmr_001):\n','s');
        findTraining=input('Are you using a training set derived from this batch of data? [yes[1]/no[0]]\n ');
    elseif length(varargin)==1
        stackName=varargin{1};
        findTraining=1;
    else
        stackName=varargin{1};
        findTraining=varargin{2};
    end
    
    if ~findTraining
        trainingSet.sameBatchFlag=zeros(length(trainingSet.spotInfo),1);
    end
    
    
    run(fullfile(pwd,'Aro_parameters.m'));
    
    myHandles.WormGaussianFitDir=WormGaussianFitDir;
    myHandles.SpotStatsDir=SpotStatsDir;
    myHandles.SegStacksDir=SegStacksDir;
    myHandles.TrainingSetDir=TrainingSetDir;
    myHandles.nestedOrFlatDirectoryStructure=nestedOrFlatDirectoryStructure;
    
    [dye, stackSuffix, wormGaussianFitName, segStacksName,spotStatsFileName]=parseStackNames(stackName);
    myHandles.findTraining=findTraining;
    myHandles.dye=dye;
    myHandles.stackSuffix=stackSuffix;
    myHandles.posNum=str2num(cell2mat(regexp(stackSuffix,'\d+','match')));
    myHandles.spotSize=[7 7];
    
    myHandles.goodColor=[.3 .3 .7];
    myHandles.badColor=[.5 .5 .1];
    
    myHandles.wormsFileName=wormGaussianFitName;
    fprintf('Loading %s ... \n',wormGaussianFitName);
    switch nestedOrFlatDirectoryStructure
        case 'flat'
            load(wormGaussianFitName);
        case 'nested'
            load(fullfile(WormGaussianFitDir,dye,wormGaussianFitName));
    end;
    myHandles.worms=worms;
    clear worms
    
    fprintf('Loading %s ... \n', spotStatsFileName)
    switch nestedOrFlatDirectoryStructure
        case 'flat'
            load(spotStatsFileName)
        case 'nested'
            load(fullfile(SpotStatsDir,dye,spotStatsFileName));
    end;
    myHandles.spotStats=spotStats;
    myHandles.spotStatsFileName=spotStatsFileName;
    clear spotStats
    
    
    
    fprintf('Loading %s ... \n', segStacksName)
    switch nestedOrFlatDirectoryStructure
        case 'flat'
            load(segStacksName)
        case 'nested'
            load(fullfile(SegStacksDir,dye,segStacksName));
    end;
    
    %Make the segStacks into long,flat stacks
    myHandles.segStacks=cell(size(segStacks));
    myHandles.wormImageMaxMerge=cell(size(segStacks));
    myHandles.laplaceWorm=cell(size(segStacks));
    myHandles.laplaceWormMaxMerge=cell(size(segStacks));
    myHandles.nSlices=zeros(size(segStacks));
    myHandles.jumps=zeros(size(segStacks));
    for si=1:length(segStacks)
        sz=size(segStacks{si});
        myHandles.nSlices(si)=sz(3);
        msz=max(sz(1:2));
        myHandles.jumps(si)=msz*2;
        myHandles.segStacks{si}=zeros(msz,2*msz*sz(3));
        myHandles.laplaceWorm{si}=zeros(size(myHandles.segStacks{si}));
        myHandles.wormImageMaxMerge{si}=zeros(msz,2*msz);
        myHandles.laplaceWormMaxMerge{si}=zeros(size(myHandles.wormImageMaxMerge{si}));
        laplacePrelim=laplaceFISH(segStacks{si},1);
        boundary=bwmorph(segMasks{si},'remove');
        for zi=1:sz(3)
            spotContextIm=imscale(segStacks{si}(:,:,zi),99.995);
            laplaceSpotContextIm=imscale(laplacePrelim(:,:,zi),99.995);
            spotContextIm=spotContextIm+.4*boundary;% if want to spend memory on RGB: cat(3,max(0,spotContextIm-.4*boundary),spotContextIm,max(0,spotContextIm-.4*boundary));
            laplaceSpotContextIm=laplaceSpotContextIm+.4*boundary;
            %set background to background of slice
            myHandles.segStacks{si}(1:msz,(1+2*msz*(zi-1)):2*msz*zi)=spotContextIm(sz(1),sz(2));%set background
            myHandles.laplaceWorm{si}(1:msz,(1+2*msz*(zi-1)):2*msz*zi)=laplaceSpotContextIm(sz(1),sz(2));%set background
            myHandles.segStacks{si}(1:sz(1),(1:sz(2))+2*msz*(zi-1))=spotContextIm;
            myHandles.laplaceWorm{si}(1:sz(1),(1:sz(2))+2*msz*(zi-1))=laplaceSpotContextIm;
        end;
        stackH=size(segStacks{si},3);
        
        myHandles.wormImageMaxMerge{si}(1:sz(1),1:sz(2))=imscale(max(segStacks{si}(:,:,floor(stackH/8):ceil(stackH*7/8)),[],3),99.995)+.4*boundary;
        myHandles.laplaceWormImageMaxMerge{si}(1:sz(1),1:sz(2))=imscale(max(laplacePrelim(:,:,floor(stackH/8):ceil(stackH*7/8)),[],3),99.995)+.4*boundary;
    end;
    myHandles.spotContext_IMPlotted=imshow(myHandles.segStacks{1},'Parent',myHandles.spotContext_axis);
    set(myHandles.spotContext_axis,'xlim',[1 msz+1]-.5,'ylim',[1 msz+1]-.5);%see note of 2015 March 24 above
    
    myHandles.arrow=line('Xdata',[1+4, 1+3, 1+4, 1+3,1+6],'Ydata',[1-1+.5, 1+.5,1+1+.5,1+.5,1+.5],'color',[1 1 1],'Visible','off','Parent',myHandles.spotContext_axis);
    
    
    
    myHandles.spotZoomRaw_IMPlotted=imshow(myHandles.segStacks{1},'Parent',myHandles.spotZoomRaw_axis);
    myHandles.spotZoomLaplaceFiltered_IMPlotted=imshow(myHandles.laplaceWorm{1},'Parent',myHandles.spotZoomLaplaceFiltered_axis);
    set(myHandles.spotZoomRaw_axis,'xlim',[1 myHandles.spotSize(2)],'ylim',[1 myHandles.spotSize(1)]);
    set(myHandles.spotZoomLaplaceFiltered_axis,'xlim',[1 myHandles.spotSize(2)],'ylim',[1 myHandles.spotSize(1)]);
    
    myHandles.surfPlots_IMPlotted=cell(size(myHandles.surfPlots_axes));
    for i=1:3
        %          myHandles.surfPlots_IMPlotted{i}=surf(myHandles.segStacks{1}(1:myHandles.spotSize(2)*2+1,1:myHandles.spotSize(1)*2+1),'Parent',myHandles.surfPlots_axes{i});
        %         set(myHandles.surfPlots_axes{i},'XLim',[1 myHandles.spotSize(2)*2+1],...
        %             'YLim',[1 myHandles.spotSize(1)*2+1],...
        %             'YDir','reverse','XTick',[],'ZLim',[0 1],'YTick',[],'ZTick',[]);
        myHandles.surfPlots_IMPlotted{i}=imshow(myHandles.segStacks{1},'Parent',myHandles.surfPlots_axes{i});
        set(myHandles.surfPlots_axes{i},'XLim',[1 myHandles.spotSize(2)*2+1],'YLim',[1 myHandles.spotSize(1)*2+1]);
        
    end;
    
    
    clear segStacks segMasks
    
    set(myHandles.fileName_button,'String', wormGaussianFitName);
    
    if ~isempty(myHandles.worms)
        fprintf('Loading %s ... \n', myHandles.spotStats{1}.trainingSetName)

        load(myHandles.spotStats{1}.trainingSetName); %directory structure already incorporated into it
        myHandles.trainingSet=trainingSet;
        clear trainingSet
    else
        disp('Breaking execution');
        return%this should break the execution
    end
    
    
    for wi=1:length(myHandles.worms)
        
        %also take care of goodWorms
        if ~isfield(myHandles.worms{wi},'goodWorm')
            myHandles.worms{wi}.goodWorm=1;
        end;
        %added nuclear information 3/31/11
        if isfield(myHandles,'nuclearInformation')
            if eq(myHandles.nuclearInformation(wi,2),-1)
                myHandles.worms{wi}.goodWorm=0;
                set(myHandles.badWorm_button,'Value',1);
                guidata(hObject,myHandles);
            end;
        end;
    end;
    
    
    myHandles.offset=floor((myHandles.spotSize-1)/2);
    myHandles.iCurrentWorm=1;
    if ~isfield(myHandles.worms{myHandles.iCurrentWorm},'spotsFixed')
        myHandles.worms{myHandles.iCurrentWorm}.spotsFixed=0;
    end;
    set(myHandles.fileName_button,'Value',myHandles.worms{myHandles.iCurrentWorm}.spotsFixed);
    myHandles=drawTheLeftPlane(myHandles);
    myHandles.nGoodToRejected=0;
    myHandles.nRejectedToGood=0;
    nGood=sum(myHandles.allLocs(:,5));
    guidata(figHandle, myHandles);
    displayImFull(figHandle,myHandles,0);
    
    % UIWAIT makes reviewFISHClassification wait for user response (see UIRESUME)
    uiwait(myHandles.figHandle);%%%function lineBox makes xdata and ydata for lines out of rectangle
    %%%position - goes clockwise from NW
    
    myHandles=guidata(figHandle);
    
    
    %rerun the randomForest with the trainingSet as it currently stands after
    %review    disp(handles);
    disp('Spot fixing done.  Saving changes');
    button = questdlg('Do you want to re-train the classifier now?','Re-train?','No');
    switch button
        case {'No'}
            
            button = questdlg('Do you want to save the training set and updated spot data when exiting?','Save when exiting?','Yes');
            switch button
                case {'No'}
                    delete(myHandles.figure1)
                case {'Yes'}
                    disp('Saving the training set and updated spot stats....')
                    trainingSet=myHandles.trainingSet;
                    save(myHandles.trainingSet.FileName,'trainingSet'); %directory structure already in it
                    worms=myHandles.worms;
                    spotStats=myHandles.spotStats;
                    switch myHandles.nestedOrFlatDirectoryStructure
                        case 'flat'
                            save(myHandles.wormsFileName,'worms');
                            save(myHandles.spotStatsFileName,'spotStats');
                        case 'nested'
                            save(fullfile(myHandles.WormGaussianFitDir,myHandles.dye,myHandles.wormsFileName),'worms');
                            save(fullfile(myHandles.SpotStatsDir,myHandles.dye,myHandles.spotStatsFileName),'spotStats');
                    end;                    disp(spotStats{1});
                    disp(myHandles.spotStatsFileName);
                    delete(myHandles.figHandle)
            end
            
        case {'Yes'}
            disp('Saving the training set and updated spot stats....')
            trainingSet=myHandles.trainingSet;
            save(myHandles.trainingSet.FileName,'trainingSet');%directory structure already in it
            worms=myHandles.worms;
            spotStats=myHandles.spotStats;
            %disp(spotStats{1});
            switch myHandles.nestedOrFlatDirectoryStructure
                case 'flat'
                    save(myHandles.wormsFileName,'worms');
                    save(myHandles.spotStatsFileName,'spotStats');
                case 'nested'
                    save(fullfile(myHandles.WormGaussianFitDir,myHandles.dye,myHandles.wormsFileName),'worms');
                    save(fullfile(myHandles.SpotStatsDir,myHandles.dye,myHandles.spotStatsFileName),'spotStats');
            end;            
            delete(myHandles.figHandle);
            disp('Rerunning randomForest with the latest amendments');
            myHandles.trainingSet=trainRFClassifier(myHandles.trainingSet);  %nameMod
            disp('Redo classification of the spots for this stack...')
            myHandles.spotStats=classifySpots(myHandles.worms,myHandles.trainingSet);
            
    end
    
    
    
    
function [xdata,ydata]=lineBox(position)
    NW=position(1:2);
    xdata=[NW(1),NW(1)+position(3),NW(1)+position(3),NW(1),NW(1)];
    ydata=[NW(2),NW(2),NW(2)+position(4),NW(2)+position(4),NW(2)];
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %displayImFull
function displayImFull(hObject,handles,drawSpotResults)
    
    data=guidata(hObject);
    set(data.nGoodToRejected_txt,'String',[num2str(data.nGoodToRejected) ' good -> rejected']);
    set(data.nRejectedToGood_txt,'String',[num2str(data.nRejectedToGood) ' rejected -> good']);
    set(data.nGoodSpots_txt,'String',[num2str(sum(data.allLocs(:,5))) ' good spots']);
    set(data.nRejectedSpots_txt,'String',[num2str(sum(data.allLocs(:,5)~=1)) ' rejected spots']);
    set(data.iCurrentWorm_txt,'String',['Specimen: ' num2str(data.iCurrentWorm) ' of ' num2str(length(data.worms))]);
    set(data.RandomForestResult_txt,'String',['Probability Estimate: ' num2str(data.allLocs(data.iCurrentSpot_allLocs,7)*100) '%']);
    set(data.scdValue_txt,'String',['scd: ' num2str(data.worms{data.iCurrentWorm}.spotDataVectors.scd(data.iCurrentSpot_worms))]);
    
    set(data.iCurrentSpot_worms_txt,'String',['Index of spot: ' num2str(data.iCurrentSpot_worms)]);
    set(data.badWorm_button,'Value',abs(data.worms{data.iCurrentWorm}.goodWorm-1));%changes good 1,0 to bad 1,0
    currentZ=data.allLocs(data.iCurrentSpot_allLocs,3);
    set(data.currentSlice_txt,'String',['Slice ' num2str(currentZ) ' of ' num2str(data.nSlices(data.iCurrentWorm))]);
    currentSlice=data.segStacks{data.iCurrentWorm}(1:size(data.segStacks{data.iCurrentWorm},1),1:size(data.segStacks{data.iCurrentWorm},1));
        
    spotBoxesTotalWidth=data.horizSideSize*data.spotSize(1);
    
    xlim(data.spotResults_axis,[colToX(1) colToX(spotBoxesTotalWidth)]);
    
    currentSpotY=data.spotBoxLocations(data.iCurrentSpot_allLocs,2);%N edge of spotBox
    spotPage=ceil(currentSpotY/(spotBoxesTotalWidth));
    set(data.spotPage_txt,'String',sprintf('Spot page %d of %d',spotPage,ceil(data.vertSideSize/data.horizSideSize)));
    
    ylim(data.spotResults_axis,[rowToY((spotPage-1)*spotBoxesTotalWidth+1) rowToY(spotPage*spotBoxesTotalWidth)]);
    
    if drawSpotResults %this doesn't appear to ever be true as of 5Mar15
        
        for si=1:size(data.allLocs,1)
            if data.allLocs(si,5)==1 %good spots
                edgeColor=[.1,.1,.5];
            else %bad spots
                edgeColor=[.5,.5,.1];
            end
            
            
            
            rectangle('Position',[data.outLines(si,:) data.spotSize],'EdgeColor',edgeColor);
            
            %for si=1:size(data.goodOutlines,1)
            %    rectangle('Position',[data.goodOutlines(si,:) data.spotSize],'EdgeColor',[.1,.1,.5]);
            %end;
            %rejected = yellow rectangles
            %for si=1:size(data.rejectedOutlines,1)
            %    rectangle('Position',[data.rejectedOutlines(si,:) data.spotSize],'EdgeColor',[.5,.5,.1]);
            %end;
            
            if data.Curated(:,3)==1 % Curated to good spot
                rectangle('Position',[data.Curated(si,1:2) data.spotSize],'EdgeColor',[0,.7,.7]);
            elseif data.Curated(:,3)==0 % Curated to bad spot
                rectangle('Position',[data.Curated(si,1:2) data.spotSize],'EdgeColor',[1,.5,0]);
            end
        end
    end
    %currentSpot
%%%    set(data.currentSpotRectangle,'Position',[data.spotBoxLocations(data.iCurrentSpot_allLocs,1)+1,data.spotBoxLocations(data.iCurrentSpot_allLocs,2)+1 data.spotSize-2]);
    %set previous rectangle handles to off
    set(data.currentSpotRedRectangleHandles(data.iPreviousSpot_allLocs),'Visible','off');
    set(data.currentSpotRedRectangleHandles(data.iCurrentSpot_allLocs),'Visible','on');
    
    %%%%%%%%%%%%%%%%%%  SPOT CONTEXT AXIS %%%%%%%%%%%%%%%%%%%%%%%%%%
    set(data.figHandle,'CurrentAxes',data.spotContext_axis);
    zoomFactor=get(data.spotContextSlider_slider,'Value');
    
    
    currentSpotZ=currentZ;
    widthOfSegStack=size(data.segStacks{data.iCurrentWorm},1);
    fullCurrentSpotX=data.spotBoxPositions(data.iCurrentSpot_allLocs,1)+data.offset(2)+2*widthOfSegStack*(currentSpotZ-1);
    if get(data.sliceMerge_button,'Value')==1
        if get(data.laplaceFilter_button,'Value')==1
            set(data.spotContext_IMPlotted,'CData',data.laplaceWorm{data.iCurrentWorm});
        else
            set(data.spotContext_IMPlotted,'CData',data.segStacks{data.iCurrentWorm});
        end;
        zOffset=2*widthOfSegStack*(currentSpotZ-1);
        xSize=size(data.segStacks{data.iCurrentWorm},2);
    else%merge
        if get(data.laplaceFilter_button,'Value')==1
            set(data.spotContext_IMPlotted,'CData',data.laplaceWormImageMaxMerge{data.iCurrentWorm});
        else
            set(data.spotContext_IMPlotted,'CData',data.wormImageMaxMerge{data.iCurrentWorm});
        end;
        
        
        zOffset=0;
        xSize=size(data.wormImageMaxMerge{data.iCurrentWorm},2);
    end;
    %  What should the xlim and ylim be?
    currentSpotX=data.spotBoxPositions(data.iCurrentSpot_allLocs,1)+data.offset(2);
    %shift currentSpotX
    currentSpotX=currentSpotX+zOffset;
    currentSpotY=data.spotBoxPositions(data.iCurrentSpot_allLocs,2)+data.offset(1);
    currContextX=max(1+zOffset,currentSpotX-.5*widthOfSegStack/(zoomFactor));
    currContextX=min(currContextX,xSize-widthOfSegStack/zoomFactor);
    currContextY=max(1,currentSpotY-.5*widthOfSegStack/(zoomFactor));
    currContextY=min(currContextY,widthOfSegStack-widthOfSegStack/zoomFactor);
    xL=[currContextX currContextX+widthOfSegStack/zoomFactor+1]-.5;
    yL=[currContextY currContextY+widthOfSegStack/zoomFactor+1]-.5;
    set(data.spotContext_axis,'xlim',xL,'ylim',yL);
    
    
    if data.allLocs(data.iCurrentSpot_allLocs,5)==1 %good spots
        arrowColor=[0,1,1];
    else %bad spots
        arrowColor=[1,1,0];
    end
    
    if get(data.arrowSpot_button,'Value')%data.arrowToSpotOnEmbryo
        set(data.arrow,'Xdata',[currentSpotX+4, currentSpotX+3, currentSpotX+4, currentSpotX+3,currentSpotX+6],'Ydata',[currentSpotY-1+.5, currentSpotY+.5,currentSpotY+1+.5,currentSpotY+.5,currentSpotY+.5],'color',arrowColor,'Visible','on');
        %rectangle('Position',[currentSpotX-7 currentSpotY-7 15 15],'EdgeColor',[.8 .4 0],'LineStyle','--');
        %Note that the rectangle was intrusive - it focused attention on the
        %putative spot and brought out its spotness even if it was no different from
        %garbage around it.  The arrow visually preserves its context
    else
        set(data.arrow,'Xdata',[currentSpotX+4, currentSpotX+3, currentSpotX+4, currentSpotX+3,currentSpotX+6],'Ydata',[currentSpotY-1+.5, currentSpotY+.5,currentSpotY+1+.5,currentSpotY+.5,currentSpotY+.5],'color',arrowColor,'Visible','off');
    end;
    
    
    %%%%%%%%%%%%%%%%%% SPOT ZOOM LAPLACE FILTERED AXIS %%%%%%%%%%%%%%%%%%
    set(data.figHandle,'CurrentAxes',data.spotZoomLaplaceFiltered_axis);
    %        disp('here7');pause;
    
    %This is from when I had it display the raw data matrix in blue with pink
    %marking the maximum
    
    
    currZoomContextX=max(1,fullCurrentSpotX-floor(data.spotSize(2)/2));
    currZoomContextX=min(currZoomContextX,size(data.segStacks{data.iCurrentWorm},2)-(data.spotSize(2)-1));
    currZoomContextY=max(1,currentSpotY-floor(data.spotSize(1)/2));
    currZoomContextY=min(currZoomContextY,size(data.segStacks{data.iCurrentWorm},1)-(data.spotSize(1)-1));
    
    xL=[currZoomContextX currZoomContextX+data.spotSize(2)];
    yL=[currZoomContextY currZoomContextY+data.spotSize(1)];
    
    set(data.spotZoomLaplaceFiltered_axis,'xlim',xL,'ylim',yL);
    %don't bother locally background subtracting?
    set(data.spotZoomRaw_axis,'xlim',xL,'ylim',yL);
    % If scaling is desired
%     rawSpotData=get(data.spotZoomRaw_IMPlotted,'CData');
%     rawSpotBoxData=rawSpotData(yToRow(yL(1):(yL(2)-1)),xToCol(xL(1):(xL(2)-1)));
%     disp(rawSpotBoxData);
%     mnBox=min(rawSpotBoxData(:));
%     mxBox=max(rawSpotBoxData(:));
%     rawSpotData=(rawSpotData-mnBox)/(mxBox-mnBox);
%     set(data.spotZoomRaw_IMPlotted,'CData',rawSpotData);
    
    
    
    
    
    %%%%%%%%%%%%%%%%%% SURF PLOTS %%%%%%%%%%%%%%%
    surfWidth=data.spotSize(2);
    surfSpan=surfWidth*2+1;
    currSurfContextX=max(1,fullCurrentSpotX-surfWidth);
    currSurfContextX=min(currSurfContextX,size(data.segStacks{data.iCurrentWorm},2)-surfSpan);
    currSurfContextY=max(1,currentSpotY-surfWidth);
    currSurfContextY=min(currSurfContextY,size(data.segStacks{data.iCurrentWorm},1)-surfSpan);
    
    xL=[currSurfContextX (currSurfContextX+surfSpan)];
    yL=[currSurfContextY (currSurfContextY+surfSpan)];
    
    %     cInds=xToCol(currSurfContextX:(currSurfContextX+surfSpan-1));
    %     rInds=yToRow(currSurfContextY:(currSurfContextY+surfSpan-1));
    %     set(data.figHandle,'CurrentAxes',data.surfPlots_axes{2});
    %         data.surfPlots_IMPlotted{2}=surf(data.segStacks{1}(rInds,cInds));
    %     set(data.figHandle,'CurrentAxes',data.surfPlots_axes{1});
    %     if currentSpotZ>1
    %         data.surfPlots_IMPlotted{1}=surf(data.segStacks{1}(rInds,cInds-data.jumps(data.iCurrentWorm)));
    %     else
    %          data.surfPlots_IMPlotted{1}=surf(zeros(size(data.segStacks{1}(rInds,cInds))));
    %     end;
    %     set(data.figHandle,'CurrentAxes',data.surfPlots_axes{3});
    %     if currentSpotZ<data.nSlices(data.iCurrentWorm)
    %         data.surfPlots_IMPlotted{3}=surf(data.segStacks{1}(rInds,cInds+data.jumps(data.iCurrentWorm)));
    %     else
    %          data.surfPlots_IMPlotted{3}=surf(zeros(size(data.segStacks{1}(rInds,cInds))));
    %     end;
    set(data.surfPlots_axes{2},'Xlim', xL,'YLim',yL);
    if currentZ>1
        set(data.surfPlots_axes{1},'Xlim', xL-data.jumps(data.iCurrentWorm),'YLim',yL);
    else
        set(data.surfPlots_axes{1},'Xlim', xL+data.jumps(data.iCurrentWorm)/2,'YLim',yL);
    end;
    if currentZ<data.nSlices(data.iCurrentWorm)
        set(data.surfPlots_axes{3},'Xlim', xL+data.jumps(data.iCurrentWorm),'YLim',yL);
    else
        set(data.surfPlots_axes{3},'Xlim', xL-data.jumps(data.iCurrentWorm)/2,'YLim',yL);
    end;
    
    
    
    
    %
    %
    %     surfWidth=data.spotSize(2);
    %     surfNR=max(1,yToRow(currentSpotY)-surfWidth);
    %     surfWC=max(1,xToCol(currentSpotX)-surfWidth);
    %     surfEC=min(origWidth,xToCol(currentSpotX)+surfWidth);
    %     surfSR=min(origHeight,yToRow(currentSpotY)+surfWidth);
    %     %fprintf('%d %d %d %d\n',surfWC,surfNR,surfEC,surfSR);
    %     surfColumn=data.segStacks{data.iCurrentWorm}(surfNR:surfSR,surfWC:surfEC,:);
    %     %freezeColors;
    %     for i=1:3
    %         if currentZ+(i-2)<=size(data.segStacks{data.iCurrentWorm},3) && currentZ+(i-2)>=1
    %             tex=sc(surfColumn(:,:,currentZ+(i-2)),'hsv');
    %             set(data.figHandle,'CurrentAxes',data.surfPlots{i});
    %             surf(surfColumn(:,:,currentZ+(i-2)),tex);
    %             set(data.surfPlots{i},'YDir','reverse','XTick',[],'ZLim',[0 1],'YTick',[],'ZTick',[],'Visible','off','Color',get(data.figHandle,'Color'));
    %
    %         end;
    %
    %     end;
    
    %this is the function that will record the center of the spot
    set(data.spotResults_axis,'ButtonDownFcn',@spotResults_ButtonDownFcn);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %guidata(hObject,data);
    
    
    
    
    
    % --- Executes on slider movement.
function spotContextSlider_Callback(hObject, eventdata)
    % hObject    handle to spotContextSlider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    structure with myData and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of
    %        slider
    
    
    myData=guidata(hObject);
    displayImFull(hObject,myData,0);
    
    
    
    % --- Executes during object creation, after setting all properties.
function spotContextSlider_CreateFcn(hObject, eventdata)
    % hObject    handle to spotContextSlider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    empty - myData not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end;
    
    
    
    % --- Executes on button press in goodSpot_button.
function goodSpot_button_Callback(hObject, eventdata)
    % hObject    handle to goodSpot_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    structure with myData and user data (see GUIDATA)
    myData=guidata(hObject);
    goodLineColor=[0 .7 .7];
    currentSpotClassification=myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,:);
    myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,3)=1;
    myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,1)=1;
    myData.allLocs(myData.iCurrentSpot_allLocs,4)=1;
    myData.allLocs(myData.iCurrentSpot_allLocs,5)=1;
    rectposition=get(myData.rectangleHandles{myData.iCurrentSpot_allLocs}.rect,'Position');
    newSpotRow=[myData.posNum myData.iCurrentWorm myData.iCurrentSpot_worms 1];
    myData.spotsCurated=[myData.spotsCurated;[newSpotRow currentSpotClassification(3)]];
    if currentSpotClassification(3)~=1%it was not manually marked as good
        disp(sprintf('Accepting rejected spot %d',myData.iCurrentSpot_worms));
        myData.nRejectedToGood=myData.nRejectedToGood+1;
        %modify image
        NW=myData.spotBoxLocations(myData.iCurrentSpot_allLocs,:);
        
        if currentSpotClassification(1)~=-1
            disp('This spot already was manually marked as bad and now we are changing it to good');
            myData.allLocs(myData.iCurrentSpot_allLocs,4)=1;
            if myData.findTraining
                [~,~,iCurrentSpot_trainingSet]=intersect(newSpotRow(1:3),myData.trainingSet.spotInfo(:,1:3),'rows'); %Check to see if the spot is in the training set
            else
                iCurrentSpot_trainingSet=[];
            end
            if isempty(iCurrentSpot_trainingSet) % not in the training set
                disp('This spot is not in the training set.  It is manually curated but not added to the training set.')
            else
                myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
                myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',goodLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
                
            end
            
        elseif get(myData.addCorrToTS_button,'Value') %it is a bad spot but was not manually marked as good and add corrections to training set button is on
            myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
            disp('This spot is added into the training set.')
            myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',goodLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
        end;
    else % It is already a good spot. Add to training set.
        disp('This spot is already classified as good spot.  Adding this spot into the training set....')
        myData.allLocs(myData.iCurrentSpot_allLocs,4)=1;
        
        myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
        disp('This spot is added into the training set.')
        myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',goodLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
    end
    myData.rectangleHandles{myData.iCurrentSpot_allLocs}.curationLine=line('Xdata',[rectposition(1)+1,rectposition(1)+myData.spotSize(1)-1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',goodLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
    set(myData.rectangleHandles{myData.iCurrentSpot_allLocs}.rect,'EdgeColor',myData.goodColor);
    myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
    
    myData.iCurrentSpot_allLocs=min(size(myData.spotBoxLocations,1),myData.iCurrentSpot_allLocs+1);
    myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    %setfocus(myData.spotResults_axis);
    %uicontrol(gcbf);
    set(myData.arrowSpot_button,'Value',1)
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    
    % --- Executes on button press in rejectedSpot_button.
function rejectedSpot_button_Callback(hObject, eventdata)
    % hObject    handle to rejectedSpot_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    structure with myData and user data (see GUIDATA)
    myData=guidata(hObject);
    badLineColor=[.7 .7 0];
    currentSpotClassification=myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,:);
    myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,3)=0;
    myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,1)=0;
    myData.allLocs(myData.iCurrentSpot_allLocs,5)=0;
    myData.allLocs(myData.iCurrentSpot_allLocs,4)=0;
    rectposition=get(myData.rectangleHandles{myData.iCurrentSpot_allLocs}.rect,'Position');
    newSpotRow=[myData.posNum myData.iCurrentWorm myData.iCurrentSpot_worms 0];
    myData.spotsCurated=[myData.spotsCurated;[newSpotRow currentSpotClassification(3)]];
    if currentSpotClassification(3)~=0
        disp(sprintf('Rejected an accepted spot %d',myData.iCurrentSpot_worms));
        myData.nGoodToRejected=myData.nGoodToRejected+1;
        %modify image
        NW=myData.spotBoxLocations(myData.iCurrentSpot_allLocs,:);
        
        if currentSpotClassification(1)~=-1
            disp('This spot already was manually marked as bad and now we are changing it to good');
            if myData.findTraining
                [~,~,iCurrentSpot_trainingSet]=intersect(newSpotRow(1:3),myData.trainingSet.spotInfo(:,1:3),'rows'); %Check to see if the spot is in the training set
            else
                iCurrentSpot_trainingSet=[];
            end
            if isempty(iCurrentSpot_trainingSet) % not in the training set
                disp('This spot is not in the training set.  It is manually curated but not added to the training set.')
            else
                myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
                myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',badLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
                
            end
            
        elseif get(myData.addCorrToTS_button,'Value') %it is a bad spot but was not manually marked as good and add corrections to training set button is on
            myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
            disp('This spot is added into the training set.')
            myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',badLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
        end;
        
    else % It is already a bad spot. Add to training set.
        disp('This spot is already classified as bad spot.  Adding this spot into the training set....')
        myData.allLocs(myData.iCurrentSpot_allLocs,4)=0;
        myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
        disp('This spot is added into the training set.')
        myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',badLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
    end
    myData.rectangleHandles{myData.iCurrentSpot_allLocs}.curationLine=line('Xdata',[rectposition(1)+1,rectposition(1)+myData.spotSize(1)-1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',badLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
    set(myData.rectangleHandles{myData.iCurrentSpot_allLocs}.rect,'EdgeColor',myData.badColor);
    myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
    
    myData.iCurrentSpot_allLocs=min(size(myData.spotBoxLocations,1),myData.iCurrentSpot_allLocs+1);
    myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    
    %setfocus(myData.spotResults_axis);
    %uicontrol(gcbf);
    set(myData.arrowSpot_button,'Value',1);
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    %--adds final spot information
function specimen = recordFinalClassification(specimen)
    %records the final spot count and adds field 'final' to the classification
    specimen.nSpotsFinal=0;
    for si=1:size(specimen.spotInfo,2)
        %remember that some of the spots were not classified but were thrown
        %out
        if isfield(specimen.spotInfo{si},'classification')
            if isfield(specimen.spotInfo{si}.classification,'manual')
                specimen.spotInfo{si}.classification.final=specimen.spotInfo{si}.classification.manual;
            else
                specimen.spotInfo{si}.classification.final=specimen.spotInfo{si}.classification.MachLearn{1};
            end;
            specimen.nSpotsFinal=specimen.nSpotsFinal+specimen.spotInfo{si}.classification.final;
        end;
    end;
    
    
    
    
    
    
    % --- Executes on button press in done_button.
function done_button_Callback(hObject, eventdata)
    % hObject    handle to done_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    
    
    myData.spotStats{myData.iCurrentWorm}.spotsFixed=1;
    myData.spotStats{myData.iCurrentWorm}=updateSpotStats(myData.spotStats{myData.iCurrentWorm});
    set(myData.arrowSpot_button,'Value',1)
    guidata(hObject,myData);
    if myData.iCurrentWorm<length(myData.worms)
        myData.iCurrentWorm=myData.iCurrentWorm+1;%go to the next specimen
        while ~myData.worms{myData.iCurrentWorm}.goodWorm%if the specimen is bad
            myData.iCurrentWorm=myData.iCurrentWorm+1;%go to the next specimen
        end
        if ~isfield(myData.worms{myData.iCurrentWorm},'spotsFixed')
            myData.worms{myData.iCurrentWorm}.spotsFixed=0;
        end
        set(myData.fileName_button,'Value',myData.worms{myData.iCurrentWorm}.spotsFixed);
        
        myData=drawTheLeftPlane(myData);
        
        nGood=sum(myData.allLocs(:,5));
        
        set(myData.spotZoomRaw_IMPlotted,'CData',myData.segStacks{myData.iCurrentWorm});
        set(myData.spotZoomLaplaceFiltered_IMPlotted,'CData',myData.laplaceWorm{myData.iCurrentWorm});
        
        for i=1:3
            set(myData.surfPlots_IMPlotted{i},'CData',myData.segStacks{myData.iCurrentWorm});
        end;
        set(myData.spotContext_IMPlotted,'CData',myData.segStacks{myData.iCurrentWorm});
        
        
        
        
        guidata(hObject, myData);
        
    else%then completely done-write training set and new spotFile,  21April2011 and goldSpots and rejectedSpots files
        
        uiresume(gcbf);
    end
    displayImFull(hObject,myData,0);
    
    % --- Executes on mouse press over axes background.
function spotResults_ButtonDownFcn(currhandle, eventdata)
    % hObject    handle to spotResults (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    structure with myData and user data (see GUIDATA)
    
    %get location of mouse click
    myData=guidata(currhandle);
    
    %disp('Mouse button clicked');
    pt = get(myData.spotResults_axis,'currentpoint');
    pixel_c=xToCol(pt(1,1));
    pixel_r=yToRow(pt(1,2));
    %disp(pt);
    %assign it to some spot
    %disp(myData.spotIndexImage);
    spotIndex=myData.spotIndexImage(pixel_r,pixel_c);
    if spotIndex>0
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        
        myData.iCurrentSpot_allLocs=spotIndex;
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    end;
    set(myData.arrowSpot_button,'Value',1)
    guidata(currhandle,myData);
    displayImFull(currhandle,myData,0);
    
    % --- Executes during object creation, after setting all properties.
function spotResults_CreateFcn(hObject, eventdata)
    % hObject    handle to spotResults (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    empty - myData not created until after all CreateFcns called
    % Hint: place code in OpeningFcn to populate spotResults
    
    % --- Executes on key press with focus on figure1 and no controls selected.
function figure1_KeyPressFcn(hObject, eventdata)
    % hObject    handle to figure1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    structure with myData and user data (see GUIDATA)
    
    %eventdata.Key
    myData=guidata(hObject);
    if strcmp(eventdata.Key,'leftarrow')
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        myData.iCurrentSpot_allLocs=max(myData.iCurrentSpot_allLocs-1,1);
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    elseif strcmp(eventdata.Key,'rightarrow')
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        myData.iCurrentSpot_allLocs=min(size(myData.spotBoxLocations,1),myData.iCurrentSpot_allLocs+1);
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    elseif strcmp(eventdata.Key,'uparrow')
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        myData.iCurrentSpot_allLocs=max(1,myData.iCurrentSpot_allLocs-myData.horizSideSize);
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    elseif strcmp(eventdata.Key,'downarrow')
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        myData.iCurrentSpot_allLocs=min(size(myData.spotBoxLocations,1),myData.iCurrentSpot_allLocs+myData.horizSideSize);
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    elseif strcmp(eventdata.Key,'pagedown')
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        myData.iCurrentSpot_allLocs=min(size(myData.spotBoxLocations,1),myData.iCurrentSpot_allLocs+(myData.horizSideSize^2)+1);
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    elseif strcmp(eventdata.Key,'pageup')
        myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
        myData.iCurrentSpot_allLocs=max(1,myData.iCurrentSpot_allLocs-(myData.horizSideSize^2));
        myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    end;
    set(myData.arrowSpot_button,'Value',1)
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    % --- Executes on key press with focus on rejectedSpot_button and none of its controls.
    % function rejectedSpot_button_KeyPressFcn(hObject, eventdata)
    %     % hObject    handle to rejectedSpot_button (see GCBO)
    %     % eventdata  structure with the following fields (see UICONTROL)
    %     %	Key: name of the key that was pressed, in lower case
    %     %	Character: character interpretation of the key(s) that was pressed
    %     %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    %     % myData    structure with myData and user data (see GUIDATA)
    %     %keypress = get(myData.figHandle,'CurrentCharacter');%myData.figure1
    %     figure1_KeyPressFcn(hObject, eventdata);
    %
    %     % --- Executes on key press with focus on goodSpot_button and none of its controls.
    % function goodSpot_button_KeyPressFcn(hObject, eventdata)
    %     % hObject    handle to goodSpot_button (see GCBO)
    %     % eventdata  structure with the following fields (see UICONTROL)
    %     %	Key: name of the key that was pressed, in lower case
    %     %	Character: character interpretation of the key(s) that was pressed
    %     %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    %     % myData    structure with myData and user data (see GUIDATA)
    %
    %     figure1_KeyPressFcn(hObject, eventdata);
    %
    %     % --- Executes on key press with focus on done_button and none of its controls.
    % function done_button_KeyPressFcn(hObject, eventdata)
    %     % hObject    handle to done_button (see GCBO)
    %     % eventdata  structure with the following fields (see UICONTROL)
    %     %	Key: name of the key that was pressed, in lower case
    %     % %	Character: characte interpretation of the key(s) that was pressed
    %     %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    %     % myData    structure with myData and user data (see GUIDATA)
    %
    %     figure1_KeyPressFcn(hObject, eventdata);
    %
    %     % --- Executes on key press with focus on redoMachLearn_button and none of its controls.
    % function redoMachLearn_button_KeyPressFcn(hObject, eventdata)
    %     % hObject    handle to rejectedSpot_button (see GCBO)
    %     % eventdata  structure with the following fields (see UICONTROL)
    %     %	Key: name of the key that was pressed, in lower case
    %     %	Character: character interpretation of the key(s) that was pressed
    %     %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    %     % myData    structure with myData and user data (see GUIDATA)
    %     %keypress = get(myData.figHandle,'CurrentCharacter');%myData.figure1
    %     figure1_KeyPressFcn(hObject, eventdata);
    %
    % --- Executes on button press in redoMachLearn_button.
function redoMachLearn_button_Callback(hObject, eventdata)
    % hObject    handle to redoMachLearn_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    myData.trainingSet=trainRFClassifier(myData.trainingSet,'runVarFeatureSel',false);  %use old variables and nFeatures
    myData.spotStats=classifySpots(myData.worms,myData.trainingSet);
    myData.iCurrentWorm=1;
    
    myData=drawTheLeftPlane(myData);
    
    nGood=sum(myData.allLocs(:,5));
    set(myData.arrowSpot_button,'Value',1)
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    
    
    % --- Executes on button press in badWorm_button.
function badWorm_button_Callback(hObject, eventdata)
    % hObject    handle to badWorm_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    myData.worms{myData.iCurrentWorm}.goodWorm=0;
    set(myData.badWorm_button,'Value',1)
    guidata(hObject,myData);
    done_button_Callback(hObject,eventdata,myData);
    
    % --- Executes on key press with focus on badWorm_button and none of its controls.
    % function badWorm_button_KeyPressFcn(hObject, eventdata)
    %     % hObject    handle to badWorm (see GCBO)
    %     % eventdata  structure with the following fields (see UICONTROL)
    %     %	Key: name of the key that was pressed, in lower case
    %     %	Character: character interpretation of the key(s) that was pressed
    %     %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    %     % myData    structure with myData and user data (see GUIDATA)
    %     %keypress = get(myData.figHandle,'CurrentCharacter');%myData.figure1
    %     figure1_KeyPressFcn(hObject, eventdata);
    
function spotPage = currentSpotPage(horizSideSize,spotSize,spotBoxLocations,iCurrentSpot)
    
    spotBoxesTotalWidth=horizSideSize*spotSize;
    currentSpotY=spotBoxLocations(iCurrentSpot,2);%N edge of spotBox
    spotPage=ceil(currentSpotY/(spotBoxesTotalWidth));
    
function m=getCurrentGoodMax(myData)
    %create vector of goodLoc values
    goodIntensities=[];
    goodIndices=find(myData.spotStatus==1);
    for ai=1:length(goodIndices)
        loc=myData.allLocs(goodIndices(ai),1:3);
        goodIntensities=[goodIntensities myData.segStacks{myData.iCurrentWorm}(loc(1),loc(2),loc(3))];
    end;
    m=max(goodIntensities);
    
    
    % --- Executes on button press in arrowSpot_button.
function arrowSpot_button_Callback(hObject, eventdata)
    % hObject    handle to arrowSpot_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of arrowSpot_button
    %checkboxStatus = 0, if the box is unchecked,
    %checkboxStatus = 1, if the box is checked
    %myData.rectangleAroundSpotOnEmbryo = get(myData.arrowSpot_button,'Value');
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    % --- Executes during object creation, after setting all properties.
function fileName_button_CreateFcn(hObject, eventdata)
    % hObject    handle to fileName_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % myData    empty - myData not created until after all CreateFcns called
    
    % --- Executes on button press in fileName_button.
function fileName_button_Callback(hObject, eventdata)
    % hObject    handle to fileName_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of fileName_button
    %if the user unchecks the handle, then the state of "spotsFixed" changes so
    %that user can redo.  the program reads the value of spotsFixed before it
    %decides to save/update or not
    if myData.worms{myData.iCurrentWorm}.spotsFixed> get(hObject,'Value')
        myData.worms{myData.iCurrentWorm}.spotsFixed=get(hObject,'Value');
    end;
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    % --- Executes on button press in addToTrainingSet_button.
function addToTrainingSet_button_Callback(hObject, eventdata)
    % hObject    handle to addToTrainingSet_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    disp('Adding this spot into the training set....')
    currentSpotClassification=myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,:);
    if currentSpotClassification(3)==1
        lineColor=[0 .7 .7];
    else
        lineColor=[.7 .7 0];
    end;
    myData.spotStats{myData.iCurrentWorm}.classification(myData.iCurrentSpot_worms,1)=currentSpotClassification(3);
    myData.allLocs(myData.iCurrentSpot_allLocs,4)=currentSpotClassification(3);
    spotIndex=[myData.posNum myData.iCurrentWorm myData.iCurrentSpot_worms];
    newSpotRow=[spotIndex currentSpotClassification(3)];
    myData.spotsCurated=[myData.spotsCurated;[newSpotRow currentSpotClassification(3)]];
    myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,newSpotRow);
    rectposition=get(myData.rectangleHandles{myData.iCurrentSpot_allLocs}.rect,'Position');
    myData.rectangleHandles{myData.iCurrentSpot_allLocs}.trainingLine=line('Xdata',[rectposition(1)+myData.spotSize(1)-1,rectposition(1)+1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',lineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
    myData.rectangleHandles{myData.iCurrentSpot_allLocs}.curationLine=line('Xdata',[rectposition(1)+1,rectposition(1)+myData.spotSize(1)-1],'Ydata',[rectposition(2)+1,rectposition(2)+myData.spotSize(2)-1],'Color',lineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
    
    %set(myData.rectangleHandles{myData.iCurrentSpot_allLocs}.rect,'EdgeColor',[0,.7,.7]); %leave the rectangle alone
    myData.iPreviousSpot_allLocs=myData.iCurrentSpot_allLocs;
    
    myData.iCurrentSpot_allLocs=min(size(myData.spotBoxLocations,1),myData.iCurrentSpot_allLocs+1);
    myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    %setfocus(myData.spotResults_axis);
    %uicontrol(gcbf);
    set(myData.arrowSpot_button,'Value',1)
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    % --- Executes on key press with focus on addToTrainingSet_button and none
    % of its controls. added 2/17/10
function addToTrainingSet_button_KeyPressFcn(hObject, eventdata)
    % hObject    handle to addToTrainingSet_button (see GCBO)
    % eventdata  structure with the following fields (see UICONTROL)
    %	Key: name of the key that was pressed, in lower case
    %	Character: character interpretation of the key(s) that was pressed
    %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    % myData    structure with myData and user data (see GUIDATA)
    
    figure1_KeyPressFcn(hObject, eventdata);
    
    % --- Executes on button press in sliceMerge_button.
function sliceMerge_button_Callback(hObject, eventdata)
    % hObject    handle to sliceMerge_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of sliceMerge_button
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    % --- Executes on key press with focus on sliceMerge_button and none of its controls.
function sliceMerge_button_KeyPressFcn(hObject, eventdata)
    % hObject    handle to sliceMerge_button (see GCBO)
    % eventdata  structure with the following fields (see UICONTROL)
    %	Key: name of the key that was pressed, in lower case
    %	Character: character interpretation of the key(s) that was pressed
    %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    % myData    structure with myData and user data (see GUIDATA)
    
    figure1_KeyPressFcn(hObject, eventdata);
    
    
    
    % --- Executes on button press in laplaceFilter_button.
function laplaceFilter_button_Callback(hObject, eventdata)
    % hObject    handle to laplaceFilter_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of laplaceFilter_button
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    
    % --- Executes on key press with focus on laplaceFilter_button and none of its controls.
    % function laplaceFilter_button_KeyPressFcn(hObject, eventdata)
    %     % hObject    handle to laplaceFilter_button (see GCBO)
    %     % eventdata  structure with the following fields (see UICONTROL)
    %     %	Key: name of the key that was pressed, in lower case
    %     %	Character: character interpretation of the key(s) that was pressed
    %     %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    %     % myData    structure with myData and user data (see GUIDATA)
    %     figure1_KeyPressFcn(hObject, eventdata);
    
    
    % --- Executes on button press in saveData_button.
function saveData_button_Callback(hObject, eventdata)
    % hObject    handle to saveData_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    myData.worms{myData.iCurrentWorm}.spotsFixed=1;
    
    disp('Saving changes');
    trainingSet=myData.trainingSet;
    save(myData.trainingSet.FileName,'trainingSet'); %directory structure already incorporated
    %%%%%%%%%%%%%%%%%%%%
    worms=myData.worms;
    spotStats=myData.spotStats;
    switch myData.nestedOrFlatDirectoryStructure
        case 'flat'
            save(myData.wormsFileName,'worms');
            save(myData.spotStatsFileName,'spotStats');
        case 'nested'
            save(fullfile(myData.WormGaussianFitDir,myData.dye,myData.wormsFileName),'worms');
            save(fullfile(myData.SpotStatsDir,myData.dye,myData.spotStatsFileName),'spotStats');
    end;
    
    disp('Data saved.')
    set(myData.arrowSpot_button,'Value',1)
    myData=drawTheLeftPlane(myData);
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    
    % --- Executes on key press with focus on saveData_button and none of its controls.
function saveData_button_KeyPressFcn(hObject, eventdata)
    % hObject    handle to saveData_button (see GCBO)
    % eventdata  structure with the following fields (see UICONTROL)
    %	Key: name of the key that was pressed, in lower case
    %	Character: character interpretation of the key(s) that was pressed
    %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    % myData    structure with myData and user data (see GUIDATA)
    figure1_KeyPressFcn(hObject, eventdata);
    
    
    % --- Executes on button press in allDone_button.
function allDone_button_Callback(hObject, eventdata)
    % hObject    handle to allDone_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    myData.spotStats{myData.iCurrentWorm}.spotsFixed=1;
    myData.spotStats{myData.iCurrentWorm}=updateSpotStats(myData.spotStats{myData.iCurrentWorm});
    %SAR - 30June2014 Added the following line
    guidata(hObject,myData);
    uiresume(gcbf);
    
    
    % --- Executes on button press in undoTheLast.
function undoTheLast_Callback(hObject, eventdata)
    % hObject    handle to undoTheLast (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    spotBeingRemoved=myData.spotsCurated(end,:);
    myData.spotStats{myData.iCurrentWorm}.classification(spotBeingRemoved(3),3)=myData.spotsCurated(end,end); % adjust back to the original state
    myData.spotStats{myData.iCurrentWorm}.classification(spotBeingRemoved(3),1)=-1;
    myData.trainingSet=updateTrainingSet(myData.trainingSet,myData.worms,spotBeingRemoved([1 2 3 5]),1); % update the training set.
    
    iSpotBeingRemoved_allLocs=find(myData.allLocs(:,6)==spotBeingRemoved(3));
    myData.spotsCurated=myData.spotsCurated(1:end-1,:);
    disp('The Spot is removed.')
    
    if isfield(myData.rectangleHandles{iSpotBeingRemoved_allLocs},'trainingLine')
        delete(myData.rectangleHandles{iSpotBeingRemoved_allLocs}.trainingLine);
    end
    
    if isfield(myData.rectangleHandles{iSpotBeingRemoved_allLocs},'curationLine')
        delete(myData.rectangleHandles{iSpotBeingRemoved_allLocs}.curationLine);
    end
    
    %delete(myData.rectangleHandles{iSpotBeingRemoved_allLocs}.rect);
    if spotBeingRemoved(end)== 1 % origianlly a good spot
        set(myData.rectangleHandles{iSpotBeingRemoved_allLocs}.rect,'EdgeColor',[0.1,0.1,0.5]);
    elseif spotBeingRemoved(end)==0
        set(myData.rectangleHandles{iSpotBeingRemoved_allLocs}.rect,'EdgeColor',[0.5,0.5,0.1]);
    end
    
    myData.nGoodToRejected=sum((myData.spotsCurated(:,5)==1).*(myData.spotsCurated(:,4)==0));
    myData.nRejectedToGood=sum((myData.spotsCurated(:,5)==0).*(myData.spotsCurated(:,4)==1));
    
    
    
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
    
    % --- Executes on button press in undoAll.
function undoAll_Callback(hObject, eventdata)
    % hObject    handle to undoAll (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    load(myData.trainingSet.FileName);
    myData.trainingSet=trainingSet;
    clear trainingSet
    
    switch myData.nestedOrFlatDirectoryStructure
        case 'flat'
            load(myData.wormsFileName);
            load(myData.spotStatsFileName);
        case 'nested'
            load(fullfile(myData.WormGaussianFitDir,myData.dye,myData.wormsFileName));
            load(fullfile(myData.SpotStatsDir,myData.dye,myData.spotStatsFileName));
    end;
    
    
    
    myData.worms{myData.iCurrentWorm}=worms{myData.iCurrentWorm};
    clear worms
    
    
    myData.spotStats{myData.iCurrentWorm}=spotStats{myData.iCurrentWorm};
    clear spotStats
    
    myData=drawTheLeftPlane(myData);
    guidata(hObject,myData);
    displayImFull(hObject,myData,0);
    
function myData=drawTheLeftPlane(myData)
    spotBoxPositions=[];
    spotBoxLocations=[];
    placeholder=-100;
    allLocs=[];
    
    placeholder=-100;
    disp('making good/rejectedLocs')
    % [Location, classification (manual), classification (final), spot index, Prob Estimates]
    allLocs=[myData.worms{myData.iCurrentWorm}.spotDataVectors.locationStack myData.spotStats{myData.iCurrentWorm}.classification(:,1) myData.spotStats{myData.iCurrentWorm}.classification(:,3), myData.worms{myData.iCurrentWorm}.spotDataVectors.spotInfoNumberInWorm myData.spotStats{myData.iCurrentWorm}.ProbEstimates];
    [allLocs,I]=sortrows(allLocs,-7); % sort the spots based on probability estimates
    allDataMat=myData.worms{myData.iCurrentWorm}.spotDataVectors.dataMat(I,:,:);
    nSpots=size(allLocs,1);
    %need next largest multiple of spotSize(1) (assume square)
    %%%6/28/09 - don't want spots to be too small...have it run off the bottom,
    %%%so maximum of, say, 30 spots per horizontal side
    
    myData.spotsPerRow=25;
    
    myData.horizSideSize=min(myData.spotsPerRow,ceil(sqrt(nSpots)));
    myData.vertSideSize=ceil(nSpots/myData.horizSideSize);
    bkgdSubImage=zeros([myData.vertSideSize myData.vertSideSize].*myData.spotSize);
    %myData.rejectedOutlines=[];%this is now going to be a list of NW corners (X,Y) for rectangles
    %myData.goodOutlines=[];%this is now going to be a list of NW corners (X,Y) for rectangles
    myData.outLines=zeros(nSpots,2);
    myData.spotIndexImage=zeros(myData.horizSideSize);%size(bkgdSubImage));
    myData.trainingSetIndex=zeros(nSpots,1); % it will be zero if it's not in the training set.
    
    %myData.outlines=.5*bwperim(ones(spotSize))+(~bwperim(ones(spotSize)));
    %myData.curated=.5*ones(spotSize);
    %myData.outlines=.3*bwperim(ones(myData.spotSize));
    %myData.curated=.3*ones(myData.spotSize);
    goodIntensities=[];
    disp(['Doing spot box locations for specimen #' num2str(myData.iCurrentWorm)]);
    for si=1:nSpots
        currentR=1+myData.spotSize(1)*floor((si-1)/myData.horizSideSize);
        currentC=1+myData.spotSize(1)*mod((si-1),myData.horizSideSize);
        % 20130518: use dataMat stored directly.  Do not use the location to
        % find the dataMat to show to avoid "index exceeds matrix" problems
        % casused by edge spots.
        
        NR=max(1,allLocs(si,1)-myData.offset(1));
        %if NR==1
        %then too close to top
        %    SR=myData.spotSize(1);
        %else
        %    if allLocs(si,1)+myData.offset(1)>size(myData.segMasks{myData.iCurrentWorm},1)
        %        SR=size(myData.segMasks{myData.iCurrentWorm},1);
        %        NR=size(myData.segMasks{myData.iCurrentWorm},1)-(myData.spotSize(1)-1);
        %    else
        %        SR=NR+(myData.spotSize(1)-1);
        %    end;
        %end;
        WC=max(1,allLocs(si,2)-myData.offset(2));
        %if WC==1
        %then too close to top
        %    EC=myData.spotSize(2);
        %else
        %    if allLocs(si,2)+myData.offset(2)>size(myData.segMasks{myData.iCurrentWorm},2)
        %        EC=size(myData.segMasks{myData.iCurrentWorm},2);
        %        WC=size(myData.segMasks{myData.iCurrentWorm},2)-(myData.spotSize(2)-1);
        %    else
        %        EC=WC+myData.spotSize(2)-1;
        %    end;
        %end;
        
        %dataMat=myData.segStacks{myData.iCurrentWorm}(NR:SR,WC:EC,allLocs(si,3));
        dataMat=permute(allDataMat(si,:,:),[2,3,1]);
        if min(dataMat(:))==0 % edge spot
            dataMat=imscale(dataMat,90);
        end
        %        rawImage(currentR:currentR+spotSize(1)-1,currentC:currentC+spotSize(2)-1)=dataMat;
        bkgdSubImage(currentR:currentR+myData.spotSize(1)-1,currentC:currentC+myData.spotSize(2)-1)=(dataMat-min(dataMat(:)))/(.2*max(allDataMat(:)));
        myData.spotIndexImage(currentR:currentR+myData.spotSize(1)-1,currentC:currentC+myData.spotSize(2)-1)=zeros(size(dataMat))+si;
        myData.outLines(si,:)=[colToX(currentC),rowToY(currentR)];
        if allLocs(si,5)==1  %good spot
            splitPoint=[rowToY(currentR+myData.spotSize(1)-1),colToX(currentC+myData.spotSize(2)-1)];%only really matters for the equality...legacy anyway
            goodIntensities=[goodIntensities dataMat(4,4)];
        end;
        spotBoxPositions=[spotBoxPositions;[colToX(WC) rowToY(NR) myData.spotSize(1) myData.spotSize(2)]];%this is for highlighting on context image
        spotBoxLocations=[spotBoxLocations;[colToX(currentC),rowToY(currentR)]];%[currentR,currentR+spotSize(1)-1,currentC,currentC+spotSize(2)-1]];%this is for finding int he spotResults image...it is NW corners in (x,y)
    end;
    
    myData.bkgdSubImage=bkgdSubImage;
    %myData.goodLocs=allLocs(allLocs(:,5)==1,:);
    %myData.rejectedLocs=allLocs(allLocs(:,5)==0,:);
    myData.allLocs=allLocs;
    myData.spotBoxPositions=spotBoxPositions;
    myData.spotBoxLocations=spotBoxLocations;
    myData.goodIntensities=goodIntensities;
    
    %myData.spotStatus=[ones(size(goodLocs,1),1);zeros(size(rejectedLocs,1),1)];%category vector
    
    myData.iCurrentSpot_allLocs=sum(allLocs(:,5));%last good spot
    if myData.iCurrentSpot_allLocs==0
        myData.iCurrentSpot_allLocs=1;
    end;
    myData.iPreviousSpot_allLocs=[];
    myData.iCurrentSpot_worms=myData.allLocs(myData.iCurrentSpot_allLocs,6);
    %myData.goodCurated(spotBoxLocations(myData.iCurrentSpot_allLocs,1):spotBoxLocations(myData.iCurrentSpot_allLocs,2),spotBoxLocations(myData.iCurrentSpot_allLocs,3):spotBoxLocations(myData.iCurrentSpot_allLocs,4))=myData.curated;
    
    %myData.worms=worms;
    
    
    %this is the function that will record the center of the spot
    set(myData.spotResults_axis,'ButtonDownFcn',@spotResults_ButtonDownFcn);
    
    %initialize spotResults figure and get myData to rectangles
    %set(myData.figHandle,'CurrentAxes',myData.spotResults_axis);
    myData.rectangleHandles={};
    myData.currentSpotRedRectangleHandles=zeros(length(nSpots),1);
    iRH=1;
    myData.spotResultsImage=imshow(myData.bkgdSubImage,'Parent',myData.spotResults_axis);%imshow(fullColor);
    set(myData.spotResultsImage,'HitTest','on');
    set(myData.spotResultsImage,'ButtonDownFcn',@spotResults_ButtonDownFcn);
    %currentSpot
    for si=1:nSpots%size(myData.goodOutlines,1)
        %myData.rectangleHandles{iRH}.rect=rectangle('Position',[myData.goodOutlines(si,:) myData.spotSize-.5],'EdgeColor',[.1,.1,.5],'HitTest','off','Parent',myData.spotResults_axis);
        if allLocs(si,5)==1
            edgeColor=myData.goodColor;
        else
            edgeColor=myData.badColor;
        end
        myData.rectangleHandles{iRH}.rect=rectangle('Position',[myData.outLines(si,:) myData.spotSize-.5],'EdgeColor',edgeColor,'HitTest','off','Parent',myData.spotResults_axis);
        myData.currentSpotRedRectangleHandles(iRH)=rectangle('Position',[myData.spotBoxLocations(si,1)+1,myData.spotBoxLocations(si,2)+1 myData.spotSize-2],'EdgeColor',[1 0 0],'HitTest','off','Parent',myData.spotResults_axis,'Visible','off');
        
        %if isequal, draw a line
        if myData.allLocs(si,4)~= -1 % then it is manually curated
            if myData.allLocs(si,5) ==1 % manually curated as a good spot
                tLineColor=myData.goodColor;
            else
                tLineColor=myData.badColor;
            end
            %myData.rectangleHandles{iRH}.trainingLine=line('Xdata',[myData.goodOutlines(si,1)+1,myData.goodOutlines(si,1)+myData.spotSize(1)-1],'Ydata',[myData.goodOutlines(si,2)+1,myData.goodOutlines(si,2)+myData.spotSize(2)-1],'Color',tLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
            myData.rectangleHandles{iRH}.curationLine=line('Xdata',[myData.outLines(si,1)+1,myData.outLines(si,1)+myData.spotSize(1)-1],'Ydata',[myData.outLines(si,2)+1,myData.outLines(si,2)+myData.spotSize(2)-1],'Color',tLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
            %set(myData.rectangleHandles{iRH}.trainingLine,'UserData',iTrainingSet);%associate trainingSetIndex
            % Check and see if the spot is in the training set. If it's in the
            % trainingSet, draw an X
            spotInfo=[myData.posNum, myData.iCurrentWorm, myData.allLocs(si,6)];
            if myData.findTraining
                [~,trainingSetIndex,~]=intersect( myData.trainingSet.spotInfo(:,1:3),spotInfo,'rows');
            else
                trainingSetIndex=[];
            end
            if ~isempty(trainingSetIndex) % order is the same as allLocs
                myData.trainingSetIndex(si)=trainingSetIndex;
                if myData.allLocs(si,5) ==1 % good spot in training set
                    tLineColor=myData.goodColor;
                else
                    tLineColor=myData.badColor;
                end
                %myData.rectangleHandles{iRH}.trainingLine=line('Xdata',[myData.goodOutlines(si,1)+1,myData.goodOutlines(si,1)+myData.spotSize(1)-1],'Ydata',[myData.goodOutlines(si,2)+1,myData.goodOutlines(si,2)+myData.spotSize(2)-1],'Color',tLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
                myData.rectangleHandles{iRH}.trainingLine=line('Xdata',[myData.outLines(si,1)+myData.spotSize(1)-1,myData.outLines(si,1)+1],'Ydata',[myData.outLines(si,2)+1,myData.outLines(si,2)+myData.spotSize(2)-1],'Color',tLineColor,'LineWidth',2,'HitTest','off','Parent',myData.spotResults_axis);
                
            end
        end
        iRH=iRH+1;
    end
%%%    myData.currentSpotRectangle=rectangle('Position',[myData.spotBoxLocations(myData.iCurrentSpot_allLocs,1)+1,myData.spotBoxLocations(myData.iCurrentSpot_allLocs,2)+1 myData.spotSize-2],'EdgeColor',[1 0 0],'HitTest','off','Parent',myData.spotResults_axis);
    set(myData.currentSpotRedRectangleHandles(myData.iCurrentSpot_allLocs),'Visible','on');
    
    
    
    % --- Executes on button press in addCorrToTS_button.
function addCorrToTS_button_Callback(hObject, eventdata)
    % hObject    handle to addCorrToTS_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    myData=guidata(hObject); % myData    structure with myData and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of addCorrToTS_button
    
